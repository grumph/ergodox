#!/bin/bash

set -u
set -e


keyboard_type=$1

k_file=keymap_${keyboard_type}.c
k_dir=qmk_firmware/keyboards/ergodox/${keyboard_type}

#TODO:
# - move to current directory
# - usage message
# - check arguments
# - submodule update --init --recursive
# - check dependencies ?


mkdir -p "${k_dir}/keymaps/grumph"
cp "$k_file" "${k_dir}/keymaps/grumph/keymap.c"
pushd "${k_dir}"
make grumph
popd
cp "qmk_firmware/ergodox_${keyboard_type}_grumph.hex" ./firmware_${keyboard_type}.hex
